# VESA mount for Acer SA230bid

| Back                  | Front                   |
| --------------------- | ----------------------- |
| ![back](doc/back.jpg) | ![front](doc/front.jpg) |

>>>
 :grey_exclamation: *.FCStd* files in this project were created using a development version of FreeCAD.
You can find the specific version after unzipping *.FCStd* inside `Document.xml` in `ProgramVersion` attribute of `Document` element.
>>>
